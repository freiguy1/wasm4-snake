# Znake

![znake-screenshot](./znake.jpg)

Play it [on GitLab pages](https://freiguy1.gitlab.io/wasm4-snake/) or [on itch.io](https://freiguy1.itch.io/znake).

This program is a game which is a recreation of the [classic snake](https://en.wikipedia.org/wiki/Snake_(video_game_genre)) game. I wrote this game mainly to learn Zig, but I also entered it in the first [WASM-4 game jam](https://itch.io/jam/wasm4).

### Running locally:

This requires Zig and wasm4 installed.

Hot reloading: `w4 watch`

Run unit tests: `zig build test`

### TODO:

- [x] Mice not step on snake while moving
- [x] Use y \* width + x as point to int conversion rather than x \<\< 5 | y
- [x] Use this new point to int conversion with anything related to snake's bitset
- [x] Draw snake head last
- [x] Keep bitset up-to-date with snake rather than creating a new one each time
- [x] Add a file for drawing snake
- [x] Choose speed
- [x] Make game area smaller
- [x] Show score
- [x] Make speeds better
- [x] Don't choose speed every game over
- [x] Make random seed start on frame # of when user picks first speed (make randomizer global?)
- [x] Mouse moves faster with faster snake speed
- [x] If mouse moves left, flip sprite, if moves right, don't
- [x] Multiple mice!
- [x] Choose number of mice

### maybe TODO:
- [ ] Random seed could be stored to disk and pulled from disk
- [ ] When mice run into walls, make random direction choose only non-walls so they keep moving
- [ ] Mice run from snake head

