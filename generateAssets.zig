const std = @import("std");
const fs = std.fs;

pub fn main() !void {
    const sprites_dir_relative_path = "sprites";
    const sprites_zig_file_relative_path = "src/raw_sprites.zig";

    // clear/delete src/sprites.zig
    const cwd = fs.cwd();
    try cwd.writeFile(sprites_zig_file_relative_path, &[0]u8{});
    var sprites_zig_file = try cwd.openFile(sprites_zig_file_relative_path, .{ .write = true });
    defer sprites_zig_file.close();

    // loop through each file in assets
    var sprites_dir = try cwd.openDir(sprites_dir_relative_path, .{ .iterate = true });
    defer sprites_dir.close();
    var walker = try sprites_dir.walk(std.heap.page_allocator);
    defer walker.deinit();
    while (try walker.next()) |entry| {
        // skip non-png
        if (entry.kind != fs.Dir.Entry.Kind.File or !std.ascii.endsWithIgnoreCase(entry.basename, ".png")) {
            continue;
        }
        var cmd_buf: [1024]u8 = undefined;
        const entry_path = try std.fmt.bufPrint(&cmd_buf, "{s}/{s}", .{ sprites_dir_relative_path, entry.path });
        var argv = [_][]const u8{ "w4", "png2src", "--zig", entry_path };
        var result = try std.ChildProcess.exec(.{ .argv = &argv, .allocator = std.heap.page_allocator });

        var line_iter = std.mem.split(u8, result.stdout, "\n");
        while (line_iter.next()) |line| {
            if (line.len == 0) continue;
            const formatted_line = try std.fmt.bufPrint(&cmd_buf, "pub {s}\n", .{line});
            try sprites_zig_file.writeAll(formatted_line);
        }
        try sprites_zig_file.writeAll("\n");
    }
}
