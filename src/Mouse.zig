const util = @import("util.zig");
const Self = @This();
const std = @import("std");
const Snake = @import("Snake.zig");
const Mouse = @import("Mouse.zig");

const Direction = util.Direction;
const Point = util.Point;
const gb_size = util.GB_HEIGHT * util.GB_WIDTH;

point: Point,
last_action_frame: usize,
frames_per_action: usize = 50,
random: std.rand.Random = undefined,
flip_sprite: bool = false,

pub fn tick(self: *Self, snake: *Snake, current_frame: usize, mice: []?Mouse) void {
    if (current_frame != self.frames_per_action + self.last_action_frame) return;

    var next_x = self.point.x;
    var next_y = self.point.y;
    const position_delta = self.random.intRangeAtMost(i3, -1, 1);

    if (self.random.boolean()) {
        next_x = @intCast(u5, std.math.clamp(@intCast(i8, self.point.x) + position_delta, 0, util.GB_WIDTH - 1));
    } else {
        next_y = @intCast(u5, std.math.clamp(@intCast(i8, self.point.y) + position_delta, 0, util.GB_HEIGHT - 1));
    }

    const snake_collision = snake.set.isSet(util.pointIntsToInt(next_x, next_y));
    const mouse_collision = for (mice) |m_opt| {
        if (m_opt) |m| {
            if (m.point.x == next_x and m.point.y == next_y) break true;
        }
    } else false;

    if (!snake_collision and !mouse_collision) {
        if (next_x < self.point.x) {
            self.flip_sprite = true;
        } else if (next_x > self.point.x) {
            self.flip_sprite = false;
        }
        self.point.x = next_x;
        self.point.y = next_y;
    }

    self.last_action_frame = current_frame;
}

pub fn generate(snake: *Snake, rng: std.rand.Random, current_frame: usize, fpa: usize, mice: []?Mouse) ?Self {
    var mice_count: u16 = 0;
    for (mice) |mouse| {
        if (mouse != null) mice_count += 1;
    }
    const empty_count: u16 = gb_size - snake.len - mice_count;

    if (empty_count == 0) return null; // no spaces left

    // var set = snake.set; // without this variable, I get a memory access out of bounds

    const random_index: u16 = rng.uintAtMost(u16, empty_count);
    var i: u16 = 0;
    var x: u5 = 0;
    var y: u5 = 0;
    while (x < util.GB_WIDTH) : (x += 1) {
        outer: while (y < util.GB_HEIGHT) : (y += 1) {
            if (snake.set.isSet(util.pointIntsToInt(x, y))) continue;
            for (mice) |m_opt| {
                if (m_opt) |m| {
                    if (m.point.x == x and m.point.y == y) continue :outer;
                }
            }
            if (i == random_index) {
                return Self{ .point = Point{ .x = x, .y = y }, .last_action_frame = current_frame, .random = rng, .frames_per_action = fpa };
            }
            i += 1;
        }
        y = 0;
    }

    return null;
}
