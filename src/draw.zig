const w4 = @import("wasm4.zig");
const Snake = @import("Snake.zig");
const sprites = @import("raw_sprites.zig");
const util = @import("util.zig");

const Point = util.Point;
const BORDER_WIDTH: u8 = 3;

pub fn drawBorders() void {
    w4.DRAW_COLORS.* = 3;
    w4.rect(0, 0, BORDER_WIDTH, w4.CANVAS_SIZE); // left
    w4.rect(0, 0, w4.CANVAS_SIZE, BORDER_WIDTH); // top
    w4.rect(w4.CANVAS_SIZE - BORDER_WIDTH, 0, BORDER_WIDTH, w4.CANVAS_SIZE); // right
    w4.rect(0, w4.CANVAS_SIZE - BORDER_WIDTH - 16, w4.CANVAS_SIZE, BORDER_WIDTH); // bottom board
    w4.rect(0, w4.CANVAS_SIZE - BORDER_WIDTH, w4.CANVAS_SIZE, BORDER_WIDTH); // bottom
}

pub fn drawScore(score: u16) void {
    w4.DRAW_COLORS.* = 4;
    const y = w4.CANVAS_SIZE - 13;
    w4.text("Score: ", 5, y);
    const score_string: [4]u8 = [4]u8{ @intCast(u8, score / 100) + 48, @intCast(u8, score / 10 % 10) + 48, @intCast(u8, score % 10) + 48, 0 };
    w4.text(score_string[0..], 55, y);
}

// -----------------------------
//           DIALOGS
// -----------------------------

pub fn drawPaused() void {
    w4.DRAW_COLORS.* = 3;
    w4.rect(39, 57, 80, 38);
    w4.DRAW_COLORS.* = 1;
    w4.rect(42, 60, 74, 32);
    w4.DRAW_COLORS.* = 4;
    w4.text("Paused", 55, 73);
}

pub fn drawGameOver() void {
    w4.DRAW_COLORS.* = 3;
    w4.rect(5, 57, 150, 38);
    w4.DRAW_COLORS.* = 1;
    w4.rect(8, 60, 144, 32);
    w4.DRAW_COLORS.* = 4;
    w4.text("   Game Over\nX: Restart\nZ: Select Options", 15, 64);
}

// -----------------------------
//           MOUSE
// -----------------------------

pub fn drawMouse(x: u5, y: u5, flip_sprite: bool) void {
    w4.DRAW_COLORS.* = 0x4230;
    const x_pixels = BORDER_WIDTH + 1 + @intCast(i32, x) * 9;
    const y_pixels = BORDER_WIDTH + 1 + @intCast(i32, y) * 9;
    var flags = w4.BLIT_2BPP;
    if (flip_sprite) flags |= w4.BLIT_FLIP_X;
    w4.blit(&sprites.mouse, x_pixels, y_pixels, 8, 8, flags);
}

// -----------------------------
//           SNAKE
// -----------------------------

// adjacency encoding TLRB in a u4
const top = 0b1000;
const left = 0b0100;
const right = 0b0010;
const bottom = 0b0001;
fn getAdjacency(segment: Snake.SegmentAndContext) u4 {
    var result: u4 = 0;
    const seg_a = segment.previous orelse Point{ .x = segment.x, .y = segment.y };
    const seg_b = segment.next orelse Point{ .x = segment.x, .y = segment.y };
    if (segment.x > seg_a.x or segment.x > seg_b.x) result |= left;
    if (segment.x < seg_a.x or segment.x < seg_b.x) result |= right;
    if (segment.y > seg_a.y or segment.y > seg_b.y) result |= top;
    if (segment.y < seg_a.y or segment.y < seg_b.y) result |= bottom;
    return result;
}

pub fn drawSnake(snake: *Snake) void {
    snake.resetIter();
    var seg_index: usize = 0;
    while (snake.nextSegment()) |segment| : (seg_index += 1) {
        if (seg_index == 0) continue; // draw snake head last
        const x = segment.x;
        const y = segment.y;
        const adjacency = getAdjacency(segment);
        if (seg_index == snake.len - 1) {
            const flags: u32 = switch (adjacency) {
                top => 0,
                left => w4.BLIT_ROTATE,
                right => w4.BLIT_ROTATE | w4.BLIT_FLIP_Y,
                else => w4.BLIT_FLIP_Y,
            };
            drawSnakeTail(x, y, flags);
        } else {
            switch (adjacency) {
                top | left => drawSnakeCorner(x, y, 0),
                top | right => drawSnakeCorner(x, y, w4.BLIT_FLIP_X),
                bottom | left => drawSnakeCorner(x, y, w4.BLIT_FLIP_Y),
                bottom | right => drawSnakeCorner(x, y, w4.BLIT_FLIP_Y | w4.BLIT_ROTATE),
                top | bottom => drawSnakeStraight(x, y, 0),
                left | right => drawSnakeStraight(x, y, w4.BLIT_ROTATE),
                else => unreachable,
            }
        }
    }

    snake.resetIter();
    const head_segment = snake.nextSegment().?;
    const adjacency = getAdjacency(head_segment);
    const flags: u32 = switch (adjacency) {
        top => 0,
        left => w4.BLIT_ROTATE,
        right => w4.BLIT_ROTATE | w4.BLIT_FLIP_Y,
        else => w4.BLIT_FLIP_Y,
    };
    drawSnakeHead(head_segment.x, head_segment.y, flags);
}

const tail = [_]u8{
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
    0b00011000,
    0b00011000,
    0b00011000,
    0b00000000,
};

const straight = [_]u8{
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
    0b00111100,
};

const corner = [_]u8{
    0b00111100,
    0b00111100,
    0b11111100,
    0b11111100,
    0b11111000,
    0b11110000,
    0b00000000,
    0b00000000,
};

fn drawSnakeHead(x: u5, y: u5, flags: u32) void {
    w4.DRAW_COLORS.* = 0x4230;
    const x_pixels = BORDER_WIDTH + 1 + @intCast(i32, x) * 9;
    const y_pixels = BORDER_WIDTH + 1 + @intCast(i32, y) * 9;
    w4.blit(&sprites.snakeHead, x_pixels, y_pixels, 8, 8, w4.BLIT_2BPP | flags);
}

fn drawSnakeTail(x: u5, y: u5, flags: u32) void {
    w4.DRAW_COLORS.* = 0x20;
    const x_pixels = BORDER_WIDTH + 1 + @intCast(i32, x) * 9;
    const y_pixels = BORDER_WIDTH + 1 + @intCast(i32, y) * 9;
    w4.blit(&tail, x_pixels, y_pixels, 8, 8, flags);
}

fn drawSnakeStraight(x: u5, y: u5, flags: u32) void {
    w4.DRAW_COLORS.* = 0x20;
    const x_pixels = BORDER_WIDTH + 1 + @intCast(i32, x) * 9;
    const y_pixels = BORDER_WIDTH + 1 + @intCast(i32, y) * 9;
    w4.blit(&straight, x_pixels, y_pixels, 8, 8, flags);
}

fn drawSnakeCorner(x: u5, y: u5, flags: u32) void {
    w4.DRAW_COLORS.* = 0x20;
    const x_pixels = BORDER_WIDTH + 1 + @intCast(i32, x) * 9;
    const y_pixels = BORDER_WIDTH + 1 + @intCast(i32, y) * 9;
    w4.blit(&corner, x_pixels, y_pixels, 8, 8, flags);
}
