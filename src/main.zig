const std = @import("std");
const w4 = @import("wasm4.zig");
const Snake = @import("Snake.zig");
const util = @import("util.zig");
const Mouse = @import("Mouse.zig");
const draw = @import("draw.zig");
const OptionSelect = @import("OptionSelect.zig");

const Point = util.Point;
const speed_options: [4][]const u8 = [_][]const u8{ "Molasses", "Kinda Movin'", "Slitherin'", "Super Slitherin'" };
const speed_option_to_snake_speed: [4]u8 = [_]u8{ 85, 170, 220, 245 };
const speed_option_to_mouse_speed: [4]u8 = [_]u8{ 80, 50, 30, 20 }; // frames per move
const mouse_num_options: [4][]const u8 = [_][]const u8{ "One mouse", "Three mice", "Ten mice", "Twenty mice" };
const mouse_num_option_num: [4]u8 = [_]u8{ 1, 3, 10, 20 };

// State
var previous_gamepad: u8 = 0;
var snake: Snake = undefined;
var current_frame: usize = 0;
var game_mode = util.GameMode.SpeedSelect;
var mice: [20]?Mouse = undefined; // [_]?Mouse{null} ** 3;
var speed_select = OptionSelect{ .options = &speed_options, .title = "Select Speed" };
var mouse_num_select = OptionSelect{ .options = &mouse_num_options, .title = "Number of Mice" };
var score: u16 = 0;
var before_first_play: bool = true;
var prng = std.rand.DefaultPrng.init(42);

fn init_before_first_play() void {
    // Setting seed of random based on frame when user starts playing
    prng.seed(@intCast(u64, current_frame));
    before_first_play = false;
}

fn init_play() void {
    var snake_speed = speed_option_to_snake_speed[speed_select.selected_index];
    snake = Snake.init(snake_speed, current_frame);
    score = 0;
    var i: usize = 0;
    var mouse_num = mouse_num_option_num[mouse_num_select.selected_index];
    for (mice) |*m| {
        if (i >= mouse_num) {
            m.* = null;
        } else {
            const mouse_speed = speed_option_to_mouse_speed[speed_select.selected_index];
            m.* = Mouse.generate(&snake, prng.random(), current_frame, mouse_speed, &mice);
        }
        i += 1;
    }
    game_mode = .Playing;
}

export fn update() void {
    //----------------------
    //    Handle Input
    //----------------------
    const pressed_this_frame = w4.GAMEPAD1.* & (w4.GAMEPAD1.* ^ previous_gamepad);

    switch (game_mode) {
        .Playing => {
            if (pressed_this_frame & w4.BUTTON_RIGHT != 0)
                snake.addNextDirection(.Right);
            if (pressed_this_frame & w4.BUTTON_LEFT != 0)
                snake.addNextDirection(.Left);
            if (pressed_this_frame & w4.BUTTON_UP != 0)
                snake.addNextDirection(.Up);
            if (pressed_this_frame & w4.BUTTON_DOWN != 0)
                snake.addNextDirection(.Down);
            if (pressed_this_frame & w4.BUTTON_1 != 0) game_mode = .Paused;
        },
        .GameOver => {
            const button_1 = pressed_this_frame & w4.BUTTON_1 != 0;
            const button_2 = pressed_this_frame & w4.BUTTON_2 != 0;

            if (button_1) {
                init_play();
            } else if (button_2) {
                game_mode = .SpeedSelect;
            }
        },
        .Paused => {
            if (pressed_this_frame & w4.BUTTON_1 != 0) game_mode = .Playing;
        },
        .SpeedSelect => {
            if (pressed_this_frame & w4.BUTTON_UP != 0) {
                speed_select.upPressed();
            } else if (pressed_this_frame & w4.BUTTON_DOWN != 0) {
                speed_select.downPressed();
            }
            if (pressed_this_frame & w4.BUTTON_1 != 0) {
                game_mode = .MouseNumSelect;
            }
        },
        .MouseNumSelect => {
            if (pressed_this_frame & w4.BUTTON_UP != 0) {
                mouse_num_select.upPressed();
            } else if (pressed_this_frame & w4.BUTTON_DOWN != 0) {
                mouse_num_select.downPressed();
            }
            if (pressed_this_frame & w4.BUTTON_1 != 0) {
                if (before_first_play) {
                    init_before_first_play();
                }
                init_play();
            }
        },
    }

    previous_gamepad = w4.GAMEPAD1.*;
    //----------------------
    //    Update State
    //----------------------
    if (game_mode == .Playing) {
        for (mice) |*m_opt| {
            if (m_opt.*) |*m| m.tick(&snake, current_frame, &mice);
        }
        if (current_frame > snake.getNextSnakeTick()) {
            const legal_tick = snake.tick(current_frame);
            if (!legal_tick) game_mode = .GameOver;
            if (snake.isEatingSelf()) game_mode = .GameOver;
        }

        for (mice) |*m_opt| {
            if (m_opt.* == null) continue;
            const m = m_opt.*.?;
            if (snake.buffer[snake.head_index] == util.pointToInt(m.point)) {
                const mouse_speed = speed_option_to_mouse_speed[speed_select.selected_index];
                m_opt.* = Mouse.generate(&snake, prng.random(), current_frame, mouse_speed, &mice);
                // Winning condition! if (m_opt.* == null) util.traceln("you win!", .{});
                snake.segments_to_add += 5;
                score += 1;
                eating_sound();
            }
        }
    }
    current_frame += 1;

    //----------------------
    //        Draw
    //----------------------
    draw.drawBorders();
    if (!before_first_play) {
        // RNG hasn't been initialized, so mouse hasn't either
        for (mice) |m_opt| {
            if (m_opt) |m| draw.drawMouse(m.point.x, m.point.y, m.flip_sprite);
        }
        draw.drawSnake(&snake);
    }
    draw.drawScore(score);
    switch (game_mode) {
        .Playing => {},
        .GameOver => draw.drawGameOver(),
        .Paused => draw.drawPaused(),
        .SpeedSelect => speed_select.draw(),
        .MouseNumSelect => mouse_num_select.draw(),
    }
}

fn eating_sound() void {
    var rng = prng.random();
    const high_pitch = rng.uintAtMost(u32, 560 - (560 - 120)) + (560 - 120);
    const low_pitch = rng.uintAtMost(u32, 160 - (160 - 40)) + (160 - 40);
    const decay = rng.uintAtMost(u32, 15 - (14 - 7)) + (14 - 7);
    w4.tone((low_pitch << 16) | high_pitch, decay << 16, 50, w4.TONE_NOISE);
}
