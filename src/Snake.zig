const Self = @This();
const util = @import("util.zig");
const std = @import("std");

const Direction = util.Direction;
const Point = util.Point;
const Set = std.StaticBitSet(util.GB_WIDTH * util.GB_HEIGHT);

pub const SegmentAndContext = struct {
    x: u5,
    y: u5,
    previous: ?Point = null,
    next: ?Point = null,
};

buffer: [util.GB_WIDTH * util.GB_HEIGHT]u9 = undefined,
current_direction: Direction = .Down,
next_direction: ?Direction = null,
next_next_direction: ?Direction = null,
head_index: u16 = 0,
iter_index: u16 = 0,
len: u16 = 0,
speed: u8 = 170,
segments_to_add: u8 = 0,
last_tick_frame: usize = 0,
set: Set = undefined,

pub fn init(spd: u8, current_frame: usize) Self {
    var self = Self{ .speed = spd, .last_tick_frame = current_frame };
    self.buffer[0] = util.pointIntsToInt(8, 4);
    self.buffer[1] = util.pointIntsToInt(8, 3);
    self.buffer[2] = util.pointIntsToInt(8, 2);
    self.buffer[3] = util.pointIntsToInt(8, 1);
    self.buffer[4] = util.pointIntsToInt(8, 0);
    self.len = 5;

    self.set = Set.initEmpty();
    while (self.nextSegment()) |segment| {
        self.set.set(util.pointIntsToInt(segment.x, segment.y));
    }
    self.resetIter();

    return self;
}

pub fn tick(self: *Self, current_frame: usize) bool {
    const next_head_index: u16 = if (self.head_index == 0) self.buffer.len - 1 else self.head_index - 1;
    const hp = util.intToPoint(self.buffer[self.head_index]); // head point
    if (self.next_direction) |nd| {
        self.current_direction = nd;
    }
    self.next_direction = self.next_next_direction;
    self.next_next_direction = null;

    const next_head_point = switch (self.current_direction) {
        .Up => blk: {
            if (hp.y == 0) return false;
            break :blk Point{ .x = hp.x, .y = hp.y - 1 };
        },
        .Down => blk: {
            if (hp.y == util.GB_HEIGHT - 1) return false;
            break :blk Point{ .x = hp.x, .y = hp.y + 1 };
        },
        .Left => blk: {
            if (hp.x == 0) return false;
            break :blk Point{ .x = hp.x - 1, .y = hp.y };
        },
        .Right => blk: {
            if (hp.x == util.GB_WIDTH - 1) return false;
            break :blk Point{ .x = hp.x + 1, .y = hp.y };
        },
    };

    self.head_index = next_head_index;
    const point_int = util.pointToInt(next_head_point);
    self.buffer[next_head_index] = point_int;
    self.set.set(point_int);

    if (self.segments_to_add != 0) {
        self.len += 1;
        self.segments_to_add -= 1;
    } else {
        // remove from bitset
        self.set.unset(self.buffer[(next_head_index + self.len) % self.buffer.len]);
    }

    self.last_tick_frame = current_frame;

    return true;
}

pub fn resetIter(self: *Self) void {
    self.iter_index = 0;
}

pub fn nextSegment(self: *Self) ?SegmentAndContext {
    if (self.iter_index == self.len) return null;
    var buffer_index: usize = (self.head_index + self.iter_index) % self.buffer.len;
    const point = util.intToPoint(self.buffer[buffer_index]);
    var result = SegmentAndContext{ .x = point.x, .y = point.y };

    if (self.iter_index != 0) {
        var previous_index: usize = (self.head_index + self.iter_index - 1) % self.buffer.len;
        result.previous = util.intToPoint(self.buffer[previous_index]);
    }

    if (self.iter_index != self.len - 1) {
        var next_index: usize = (self.head_index + self.iter_index + 1) % self.buffer.len;
        result.next = util.intToPoint(self.buffer[next_index]);
    }

    self.iter_index += 1;
    return result;
}

pub fn addNextDirection(self: *Self, d: Direction) void {
    if (self.next_direction == null) {
        if (self.current_direction != d.opposite()) self.next_direction = d;
    } else if (self.next_next_direction == null) {
        if (self.next_direction.? != d.opposite()) self.next_next_direction = d;
    }
}

pub fn isEatingSelf(self: Self) bool {
    var i: usize = 1;
    while (i < self.len) : (i += 1) {
        var buffer_index: usize = (self.head_index + i) % self.buffer.len;
        if (self.buffer[buffer_index] == self.buffer[self.head_index]) {
            return true;
        }
    }
    return false;
}

pub fn getNextSnakeTick(self: Self) usize {
    const denominator = 256.0 / (util.MAX_FRAMES_PER_SNAKE_TICK - util.MIN_FRAMES_PER_SNAKE_TICK);
    const frames_per_tick = (@intToFloat(f64, 255 - self.speed) / denominator) + util.MIN_FRAMES_PER_SNAKE_TICK;
    const elapsed_ticks = std.math.divCeil(f64, @intToFloat(f64, self.last_tick_frame), frames_per_tick) catch 0.0;
    return @floatToInt(usize, @round(elapsed_ticks * frames_per_tick));
}
