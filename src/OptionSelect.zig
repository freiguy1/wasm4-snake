const Self = @This();
const std = @import("std");
const w4 = @import("wasm4.zig");

const mid_canvas = @intCast(u8, w4.CANVAS_SIZE / 2);
const font_height: u8 = 8;
const margin: u8 = 2;

options: []const []const u8 = undefined,
selected_index: u8 = 0,
title: ?[]const u8 = null,

pub fn draw(self: Self) void {
    const max_char_width = self.calcMaxCharWidth();
    const content_width = max_char_width * font_height;
    const width = content_width + margin * 2;
    const box_left = mid_canvas - (width / 2);
    const line_count = if (self.title != null) 1 + self.options.len else self.options.len;
    const height = @intCast(u8, margin + ((font_height + margin) * line_count));
    const box_top = @intCast(i32, mid_canvas - (height / 2));
    w4.DRAW_COLORS.* = 0x02;
    w4.rect(box_left, box_top, w4.CANVAS_SIZE - box_left - box_left, height);

    const title_height: u8 = if (self.title) |title| blk: {
        w4.DRAW_COLORS.* = 0x14;
        drawStringCenter(title, box_left + margin, box_top + margin, max_char_width);
        break :blk 10;
    } else 0;

    for (self.options) |option, index| {
        if (index == self.selected_index) w4.DRAW_COLORS.* = 0x14 else w4.DRAW_COLORS.* = 0x24;
        drawStringCenter(option, box_left + margin, @intCast(u8, index) * (margin + font_height) + box_top + margin + title_height, max_char_width);
    }
}

fn drawStringCenter(string: []const u8, x: i32, y: i32, max_char_width: u8) void {
    var buf: [32]u8 = [_]u8{' '} ** 32;
    buf[max_char_width] = 0;
    const left_spaces = (max_char_width - string.len) / 2;
    std.mem.copy(u8, buf[left_spaces..], string);
    w4.text(buf[0..], x, y);
}

fn calcMaxCharWidth(self: Self) u8 {
    var max_char_width: u8 = 0;
    for (self.options) |o| {
        max_char_width = std.math.max(@intCast(u8, o.len), max_char_width);
    }

    if (self.title != null and self.title.?.len > max_char_width) {
        max_char_width = @intCast(u8, self.title.?.len);
    }

    return max_char_width;
}

pub fn downPressed(self: *Self) void {
    self.selected_index = (self.selected_index + 1) % @truncate(u8, self.options.len);
}

pub fn upPressed(self: *Self) void {
    self.selected_index = std.math.min(self.selected_index -% 1, self.options.len - 1);
}
