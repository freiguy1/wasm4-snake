// Consts & simple structs/enums
const std = @import("std");
const w4 = @import("wasm4.zig");

// Constants
pub const GB_WIDTH = 17;
pub const GB_HEIGHT = 15;
pub const MIN_FRAMES_PER_SNAKE_TICK = 3.0;
pub const MAX_FRAMES_PER_SNAKE_TICK = 28.0;

pub const GameMode = enum { Playing, GameOver, Paused, SpeedSelect, MouseNumSelect };

pub const Direction = enum {
    Up,
    Down,
    Left,
    Right,
    pub fn opposite(self: Direction) Direction {
        return switch (self) {
            .Up => .Down,
            .Down => .Up,
            .Left => .Right,
            .Right => .Left,
        };
    }
};

pub const Point = struct { x: u5, y: u5 };

pub fn pointIntsToInt(x: u5, y: u5) u9 {
    return @intCast(u9, y) * GB_WIDTH + x;
}

pub fn pointToInt(p: Point) u9 {
    return pointIntsToInt(p.x, p.y);
}

pub fn intToPoint(p: u9) Point {
    return Point{ .x = @truncate(u5, p % GB_WIDTH), .y = @truncate(u5, p / GB_WIDTH) };
}

pub fn traceln(comptime fmt: []const u8, args: anytype) void {
    var printBuf: [1024]u8 = [_]u8{0} ** 1024;
    const printSlice = std.fmt.bufPrint(&printBuf, fmt, args) catch "error";
    w4.trace(printSlice.ptr);
}

test "point conversions" {
    try std.testing.expect(pointIntsToInt(3, 0) == 3);
    try std.testing.expect(pointIntsToInt(3, 1) == 20);

    const p = Point{ .x = 10, .y = 13 };
    // back and forth
    try std.testing.expect(std.meta.eql(p, intToPoint(pointToInt(p))));
}
